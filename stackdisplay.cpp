#include <iostream>

using namespace std;

const int ukuran = 4;

struct dataStack
{
    int top;
    int data[ukuran];
} stack;

void buatStack()
{
    stack.top = -1;
}

int kosong()
{
    if (stack.top == ukuran)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int penuh()
{
    if (stack.top == ukuran - 1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void pushStack(int nilai)
{
    if (penuh() == 0)
    {
        stack.top += 1;
        stack.data[stack.top] = nilai;
        cout << "Data : " << stack.data[stack.top] << endl;
    }
    else
    {
        cout << "Stack sudah penuh gaes" << endl;
    }
}

// Menampilkan data
void display()
{
    for (int i = 0; i <= stack.top; i++)
    {
        // cout << i << endl;
        cout << stack.data[i] << " ";
    }
}

int main()
{
    buatStack();
    pushStack(3);
    pushStack(4);
    pushStack(5);
    pushStack(6);
    display();
}