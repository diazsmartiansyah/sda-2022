#include <iostream>

using namespace std;

const int ukuran = 100;

struct dataStack
{
    int top;
    char nama[ukuran];
} stack;

void buatStack()
{
    stack.top = -1;
}

int penuh()
{
    if (stack.top == ukuran - 1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void pushStack(char nilai)
{
    if (penuh() == 0)
    {
        stack.top += 1;
        stack.nama[stack.top] = nilai;
        cout << "Data : " << stack.nama[stack.top] << endl;
    }
    else
    {
        cout << "Stack sudah penuh gaes" << endl;
    }
}

// Menampilkan data
void display()
{
    for (int i = 0; i <= stack.top; i++)
    {
        // cout << i << endl;
        cout << stack.nama[i] << " ";
    }
}

// Menampilkan data terbalik
void displayReverse()
{
    for (int i = stack.top; i >= 0; i--)
    {
        cout << stack.nama[i] << " ";
    }
}

int main()
{
    buatStack();
    pushStack('D');
    pushStack('i');
    pushStack('a');
    pushStack('z');
    pushStack('s');

    pushStack('M');
    pushStack('a');
    pushStack('r');
    pushStack('t');
    pushStack('i');
    pushStack('a');
    pushStack('n');
    pushStack('s');
    pushStack('y');
    pushStack('a');
    pushStack('h');
    cout << "Sebelum Dibalikan" << endl;
    display();

    cout << "\nSesudah Dibalikan" << endl;
    displayReverse();
}